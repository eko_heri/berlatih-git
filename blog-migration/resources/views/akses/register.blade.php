<!doctype html>
<html lang="en">
<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

    <div class="container">
        <div class="header">
            <h1>Buat Account Baru!</h1>
            <h2>Sign Up Form</h2>
        </div>
        <div class="content">
            <form action="/kirim" method="POST">
                @csrf

                <!--Form Nama-->
                <label for="fname">First Name :</label><br>
                <input type="text" name="fname" required />
                <br>
                <label for="lname">Last Name :</label><br>
                <input type="text" name="lname" required />
                <br><br>
                <!--Form Gender-->
                <label>Gender :</label><br><br>
                <input type="radio" id="gdr" name="gender" required value="Male">
                <label for="gdr">Male</label><br>
                <input type="radio" id="gdr" name="gender" value="Female">
                <label for="gdr">Female</label><br>
                <input type="radio" id="gdr" name="gender" value="Female">
                <label for="gdr">Other</label><br><br>

                <!--Form Nationality-->
                <label>Nationality :</label><br><br>
                <select id="ntl" required>
                    <option value="idn">Indonesia</option>
                    <option value="mys">Malaysia</option>
                    <option value="spore">Singapore</option>

                </select>
                <br><br>
                <!--Form Langguage Spoken-->
                <label>Langguage Spoken :</label>
                <br><br>
                <input type="checkbox" name="e-bhs" required value="bhs">
                <label for="e-bhs">Bahasa Indonesia</label><br>
                <input type="checkbox" name="e-bhs" value="bhs">
                <label for="e-bhs">English</label><br>
                <input type="checkbox" name="e-bhs" value="bhs">
                <label for="e-bhs">Other</label><br><br>
                <!--Form Biografi-->
                <label>Bio :</label><br>
                <textarea name="bio" rows="7" cols="50"></textarea>
                <br>
                <input type="submit" value="Kirim">
            </form>
        </div>


        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
