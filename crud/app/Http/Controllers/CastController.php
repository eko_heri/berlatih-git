<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function index(){
        $show=DB::table('cast')->get();
        return view('items.cast',compact('show'));
    }

    public function create(){
        return view('items.create'); 
    }

    public function store(Request $request){
        dd($request->all());
    }
}
