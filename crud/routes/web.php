<?php
// Page master
Route::get('/master',function(){
    return view('adminlte.master');
});

// Bekerja dg cast
Route::get('/cast','CastController@index');
Route::get('/cast/create','CastController@create');
Route::post('/cast','CastController@store');


//Page Lain
Route::get('/items/index',function(){
    return view('items.index');
});

// Halaman dasboard
Route::get('/',function(){
    return view('items.index');
});
Route::get('/table',function(){
    return view ('table.table');
});
Route::get('data-table',function(){
    return view ('table.data-table');
});
