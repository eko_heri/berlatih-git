@extends('adminlte.master')


@section('content')
<h1>Tampil list cast </h1>
<a href="/cast/create" class="btn btn-primary" role="button"><i class="fa fa-plus" aria-hidden="true"></i> Create</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Created</th>
        <th scope="col">Update</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($show as $key=>$show)
      <tr>
        {{-- <th scope="row">1</th> --}}
        <td>{{ $key+1 }}</td>
        <td>{{ $show->nama }}</td>
        <td>{{ $show->umur }}</td>
        <td>{{ $show->bio }}</td>
        <td>{{ $show->created_at }}</td>
        <td>{{ $show->updated_at }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
@endsection

@push('scripts')


@endpush
