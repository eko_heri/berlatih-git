<aside class="main-sidebar sidebar-dark-primary elevation-6">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <img src="{{asset('/adminlte/dist/img/AdminLTELogo.png')}}" alt="Admin" class="brand-image img-circle elevation-3" style="opacity: .5">
        <span class="brand-text font-weight-light">Web Programing </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('/adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">echohere.id</a>
            </div>
        </div>

        <!-- SidebarSearch Form -->
        <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
   with font-awesome or any other icon font library -->


                <li class="nav-item">
                    <a href="/" class="nav-link">
                        <i class="nav-icon fab fa-blackberry"></i>
                        <p>Dashboard</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="/table" class="nav-link">
                        <i class="nav-icon fas fa-table"></i>
                        <p>Table <i class="nav-item"></i></p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="/data-table" class="nav-link">
                        <i class="nav-icon fas fa-mug-hot "></i>
                        <p>Data Table <i class="nav-item"></i></p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="/cast" class="nav-link">
                        <i class="nav-icon fa fa-users aria-hidden="true"></i>
                        <p>Peran <i class="nav-item"></i></p>
                    </a>
                </li>
            </ul>
            </li>



            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
