<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('akses.register');
    }

    public function send(Request $request){
        $first=$request['fname'];
        $bio=$request['bio'];

        return view('akses.dashboard',compact('first','bio'));
    }
}
