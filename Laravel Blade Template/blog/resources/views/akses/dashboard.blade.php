<!doctype html>
<html lang="en">
<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <h1>Selamat Datang,{{$first}} </h1>
    <hr>
    <h3>Terima Kasih telah bergabung
        di sanberbook. Social Media kita bersama!</h3>
    <hr>
    <h5>Biografi :</h5>
    {{ $bio }}
    <hr>
    <a href="/register">Back to Register</a>
</body>
</html>
