<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


#Register admin
// Route::get('/', 'HomeController@home');
// Route::get('/register', 'AuthController@register');
// Route::post('/kirim', 'AuthController@send');

// ======================================
//Master
Route::get('/master',function(){
    return view('adminlte.master');
});


Route::get('/items/create',function(){
    return view('items.create');
});

Route::get('/items/index',function(){
    return view('items.index');
});

Route::get('/',function(){
    return view('items.index');
});

Route::get('/table',function(){
    return view ('table.table');
});


Route::get('data-table',function(){
    return view ('table.data-table');
});
