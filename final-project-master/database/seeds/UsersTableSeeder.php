<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'id' => 1,
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'password' => Hash::make('admin'),
                'role' => 'Admin'
            ],
            [
                'id' => 2,
                'name' => 'Sunandar',
                'email' => 'sunandar@gmail.com',
                'password' => Hash::make('sunandar'),
                'role' => 'Customer'
            ],
            [
                'id' => 3,
                'name' => 'Ilham',
                'email' => 'ilham@gmail.com',
                'password' => Hash::make('ilham'),
                'role' => 'Customer'
            ],
            [
                'id' => 4,
                'name' => 'Bim bim',
                'email' => 'bim_bim@gmail.com',
                'password' => Hash::make('bimbim'),
                'role' => 'Customer'
            ],
        ]);
    }
}
