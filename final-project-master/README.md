### Installation

1. Clone the repository using the command "git clone [link]"
2. Create database in MySql
3. Configure the .env file accordingly
4. Run command

```
$composer install
$php artisan migrate
$php artisan db:seed
$php artisan serve
$php artisan storage:link
```

### Built With

-   Bootstrap- CSS framework
-   JQuery- Javascript framework
-   Laravel - PHP framework
-   MySql- Databse

### Nama Kelompok

-   Isa Afisyah Jamil
-   Eko Heri Susanto
-   Ramdhani

### ERD

![final](/uploads/fb9d15999ad8986a572681bea53fe698/final.png)

### Template

Colorlib.
